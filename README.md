# Simple Java IO library to get Strings or any primitive data type from the user.

## Setup
Import the jar as a library.
Add it as a dependency under the scope _compile_ if this has not been done automatically.

## Info
* An example on how to use this library is seen at Example.java
* Available **static** classes.
  * #### Blocking:
    * This Class contains blocking IO functions that return their type when the set requirements are met.
    * The return-types are set by the user.
    * Filters on these return-types can be applied by using the **Filter** class.
    * If only one filter is desired, the function _getString(int selection, bool canBeEmpty)_ can be called.
      This selection parameter selects one filter.
    * Adding more filters can be applied manually with the **Filter** class.
    * #### Blocking.FUNCTIONNAME(PARAMS);
  * #### Filter:
      * This class contain functions that apply a filter on given input and returns it.
      * The return-types are set with the given parameter and does not contain user handling.
        * #### Filter.FUNCTIONNAME(PARAMS);


# Functions
All available functions are listed here:
## Blocking (IO)
### get Bool
* boolean getBool()

        - 'YES'/'yes'/'Y'/'y'/'1'/empty_string = True.
        - 'NO'/'no'/'N'/'n'/'0' = False.

        @return true or false.

### get Index
#### Multiple Choice
* int **getIndex**(int amountOfOptions)

        - This method offers multiple choice.
        - Give '-1' as wildcard for any positive number (0 to MAX_INT).
        - If e.g. '5' is given: Return an int ranging from 0 to 4.

        @param *amountOfOptions* Amount of options the user can choose from.
        @return A positive integer. Returns '-1' if parameter is invalid.

* int **getIndex**(int minScope, int maxScope)

        - This method offers multiple choice.
        - Give '-1' as wildcard for an unlimited scope (MIN_INT, MAX_INT).
        - E.g. If '1' and '6' is given: Return an int ranging from 1 to 5.
  
        @param *minScope* Starting from this int, '-1' for MIN_INT.
        @param *maxScope* Up to this int, '-1' for MAX_INT.
        @throws InvalidScope Given scope is smaller than 2.
        @return An integer in given scope. Returns '-1' if parameters are invalid.

### get Number
#### Type java.lang.Number
* Number **getNumber**(Class<?> type)

        - Get a number of class java.lang.Number
        - E.g. int i = (Integer) userInputIO.getNumber(Integer.class)

        @param *type* Any variable of type Number (int, float, etc...)
        @return Number of parameter type, returns 0 if invalid.

### get String
* String **getString**(boolean canBeEmpty)

        - Gets string from user input.
        - Filters can be accessed separately in Filter class.

        @param *canBeEmpty* If a String may be longer than 0 characters.
        @return A String.

* String **getStringLimited**(int limit, boolean canBeEmpty)

        - Gets string from user input of less than or equal to LIMIT characters.

        @param *limit* The maximum length a string may have.
        @param *canBeEmpty* If a String may be longer than 0 characters.
        @return A String filtered on length.

## Filters
### Only Letters
#### Filter illegal characters, numbers, spaces and symbols.
* char[] **onlyLetters**(char[] input)
* String **onlyLetters**(String input)

        - Only allow letters in a char-array / String.

        @param *input* char-array / String that may only consist letters.
        @return filtered word.

### Only Letters and Spaces
#### Filter illegal characters, numbers and symbols.
* char[] **onlyLettersAndSpaces**(char[] input)
* String **onlyLettersAndSpaces**(String input)

        - Only allow letters and spaces in a char-array / String.

        @param *input* char-array / String that may only consist letters and spaces.
        @return filtered word.

### Only Letters and Numbers
#### Filter illegal characters and symbols.
* char[] **onlyLettersAndNumbers**(char[] input)
* String **onlyLettersAndNumbers**(String input)

        - Only allow letters and numbers in a char-array / String.

        @param *input* char-array / String that may only consist letters and numbers.
        @return filtered word.

### No Symbols
#### Filters illegal characters and symbols.
* char[] **noSymbols**(char[] input)
* String **noSymbols**(String input)

        - Only allow letters, numbers and spaces in a char array.

        @param input char array that may only consist letters, numbers and spaces.
        @return filtered word.

### No Spaces
#### Filter spaces
* char[] **noSpaces**(char[] input)
* String **noSpaces**(String input)

        - Remove all spaces.

        @param input char-array / String that may not consist spaces.
        @return filtered word.

### No Duplicate Characters
* char[] **noDuplicates**(char[] input, bool caseSensitive)
* String **noDuplicates**(String input, bool caseSensitive)

        - Every char in the char-array / String can only be allowed once.
          If caseSensitive; one lowercase and one uppercase per letter is allowed.

        @param *input* char-array / String that may not contain duplicates.
        @return a char-array / String where each character exists once.

### Only One Word
* char[] **onlyOneWord**(char[] input)
* String **onlyOneWord**(String input)

        - Removes any chars after the first space.

        @param input char-array / String that may only contain one word.
        @return filtered sentence to first word.

### Capitalize / Un-capitalize letters
* char[] **capitalize**(char[] input)
* String **capitalize**(String input)

        - Capitalizes all letters in a char[] array.

        @param input char array that needs capitalization.
        @param capitalize True = Capitalize, False = Un-capitalize.
        @return capitalized array.