package org.userio.Example;

import org.userio.Blocking;
import org.userio.Exceptions.InvalidScope;
import org.userio.Filter;

/** Example class that uses available methods in psvm.
 * Any method that requires IO will return a string.
 * Filtering methods can be called with either a String or a char array.
 */
public class Example{

	static String[] str = new String[8];
	static boolean hasPet;
	static int[] num = new int[5];
	static float f;
	public static void main(String[] args) throws InvalidScope {

		setVariables();
		printVariables();
	}

	private static void setVariables() throws InvalidScope {

		//Strings must have at least one character
		boolean canBeEmpty = false;
		boolean isCaseSensitive = true;
		boolean toUpperCase = true;

		/*---Strings---*/

		//Only one word and only letters
		//Getting a String from IO can be performed with 0 or 1 filter.
		//Adding another filter requires calling it consecutively with given String.
		System.out.println("Your first name:");
		str[0] = Filter.onlyOneWord(Blocking.getString(!canBeEmpty));
		str[0] = Filter.onlyLetters(str[0]);

		//Only letters and spaces
		System.out.println("Your full name:");
		str[1] = Filter.onlyLettersAndSpaces(Blocking.getString(canBeEmpty));

		//Only letters and numbers
		System.out.println("A String with no spaces nor symbols:");
		str[2] = Filter.onlyLettersAndNumbers(Blocking.getString(canBeEmpty));

		//Only letters, numbers and spaces
		System.out.println("A String with no illegal characters:");
		str[3] = Filter.noSymbols(Blocking.getString(canBeEmpty));

		//No spaces
		System.out.println("A String that filters spaces:");
		str[4] = Filter.noSpaces(Blocking.getString(canBeEmpty));

		//No duplicates
		System.out.println("A String that filters duplicate letters:");
		str[5] = Filter.noDuplicates(Blocking.getString(canBeEmpty), isCaseSensitive);

		//String with a limited size
		System.out.println("A String with a maximum size of 6 characters:");
		str[6] = Blocking.getStringLimited(6, canBeEmpty);

		//Capitalizes all letters
		System.out.println("A String with capitalised letters:");
		str[7] = Filter.capitalize(Blocking.getString(canBeEmpty), toUpperCase);

		/*---Boolean---*/

		//Closed Question
		System.out.println("\nDo you have a pet?");
		hasPet = Blocking.getBool();

		/*---Integers---*/

		//Any num
		System.out.println("\nAny num:");
		num[0] = Blocking.getIndex(-1, -1);

		//Any positive num
		System.out.println("\nAny positive num:");
		num[1] = Blocking.getIndex(-1);
		//OR Blocking.getIndex(0, -1);

		//Any negative num
		System.out.println("\nAny negative num:");
		num[2] = Blocking.getIndex(-1, 0);

		//Multiple choice num (zero-based)
		System.out.println("\nMultiple choice with 5 options (zero based):");
		num[3] = Blocking.getIndex(5);

		//Multiple choice num (zero-based)
		System.out.println("\nMultiple choice with 5 options (not zero based, more user friendly):");
		num[4] = Blocking.getIndex(1,6);

		//Number from java.lang.Number, Like Int, Float etc...
		System.out.println("\nA Float");
		f = (float) Blocking.getNumber(Float.class);

		}


	private static void printVariables(){
		System.out.print("Your first name (Only letters, Only one word): ");
		System.out.println(str[0]);
		System.out.print("Your full name (Only letters and spaces): ");
		System.out.println(str[1]);
		System.out.print("String with only letters and numbers: ");
		System.out.println(str[2]);
		System.out.print("No symbols (Only letters, numbers and spaces): ");
		System.out.println(str[3]);
		System.out.print("No spaces: ");
		System.out.println(str[4]);
		System.out.print("No duplicates: ");
		System.out.println(str[5]);
		System.out.print("String with limited length: ");
		System.out.println(str[6]);
		System.out.print("String with capitalized letters: ");
		System.out.println(str[7]);

		System.out.print("Has a pet: " );
		System.out.println(hasPet);

		System.out.print("Any number: ");
		System.out.println(num[0]);
		System.out.print("Any positive number: ");
		System.out.println(num[1]);
		System.out.print("Any negative number: ");
		System.out.println(num[2]);
		System.out.print("Multiple choice (zero based): ");
		System.out.println(num[3]);
		System.out.print("Multiple choice (not zero based): ");
		System.out.println(num[4]);

		System.out.print("A float: ");
		System.out.println(f);
	}
}